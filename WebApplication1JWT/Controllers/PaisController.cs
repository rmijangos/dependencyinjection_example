﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1JWT.Models;
using WebApplication1JWT.Services;
using WebApplication1JWT.Services.Interfaces;

namespace WebApplication1JWT.Controllers
{
    [Authorize]
    public class PaisController : ApiController

    {
        IPaisService paisService;
        public PaisController(IPaisService paisService)
        {
            this.paisService = paisService;
        }

        // GET: api/Pais
        public List<Pais> GetPais()
        {
            return paisService.GetPaises();
        }

        // GET: api/Pais/5
        [ResponseType(typeof(Pais))]
        public IHttpActionResult GetPais(int id)
        {
            Pais pais = paisService.GetPaisByID(id);
            if (pais == null)
            {
                return NotFound();
            }

            return Ok(pais);
        }

        // PUT: api/Pais/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPais(int id, Pais pais)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pais.Id)
            {
                return BadRequest();
            }

            bool wasUpdated = paisService.UpdatePaisByID(id, pais);

            if (!wasUpdated) return NotFound();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pais
        [ResponseType(typeof(Pais))]
        public IHttpActionResult PostPais(Pais pais)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            paisService.PostPais(pais);

            return CreatedAtRoute("DefaultApi", new { id = pais.Id }, pais);
        }

        // DELETE: api/Pais/5
        [ResponseType(typeof(Pais))]
        public IHttpActionResult DeletePais(int id)
        {
            Pais pais = paisService.GetPaisByID(id);
            if (pais == null)
            {
                return NotFound();
            }

            paisService.DeletePais(pais);

            return Ok(pais);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                paisService.Dispose();
            }
            base.Dispose(disposing);
        }

        
    }
}