﻿using WebApplication1JWT.Models;
using WebApplication1JWT.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace WebApplication1JWT.Services
{
    public class PaisService : IPaisService
    {
        private Database1Entities db;
        public PaisService(Database1Entities db)
        {
            //db = new Database1Entities();
            this.db = db;
        }
        public List<Pais> GetPaises()
        {
            return db.Pais.ToList();
        }
        public Pais GetPaisByID(int id)
        {
            return db.Pais.Find(id);
        }

        public bool UpdatePaisByID(int id, Pais pais)
        {
            db.Entry(pais).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaisExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
            return true;
        }

        public void PostPais(Pais pais)
        {
            db.Pais.Add(pais);
            db.SaveChanges();
        }

        public void DeletePais(Pais pais)
        {
            db.Pais.Remove(pais);
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        private bool PaisExists(int id)
        {
            return db.Pais.Count(e => e.Id == id) > 0;
        }

    }
}