﻿using WebApplication1JWT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1JWT.Services.Interfaces
{
    public interface IPaisService
    {
        List<Pais> GetPaises();
        Pais GetPaisByID(int id);
        void PostPais(Pais pais);
        bool UpdatePaisByID(int id, Pais pais);
        void DeletePais(Pais pais);
        void Dispose();
    }
}
